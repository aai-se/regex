package com.automationanywhere.botcommand.demo;

import com.automationanywhere.bot.service.GlobalSessionContext;
import com.automationanywhere.botcommand.BotCommand;
import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import java.lang.ClassCastException;
import java.lang.Deprecated;
import java.lang.Object;
import java.lang.String;
import java.lang.Throwable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class GetNumberOfMatchesCommand implements BotCommand {
  private static final Logger logger = LogManager.getLogger(GetNumberOfMatchesCommand.class);

  private static final Messages MESSAGES_GENERIC = MessagesFactory.getMessages("com.automationanywhere.commandsdk.generic.messages");

  @Deprecated
  public Optional<Value> execute(Map<String, Value> parameters, Map<String, Object> sessionMap) {
    return execute(null, parameters, sessionMap);
  }

  public Optional<Value> execute(GlobalSessionContext globalSessionContext,
      Map<String, Value> parameters, Map<String, Object> sessionMap) {
    logger.traceEntry(() -> parameters != null ? parameters.entrySet().stream().filter(en -> !Arrays.asList( new String[] {}).contains(en.getKey()) && en.getValue() != null).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)).toString() : null, ()-> sessionMap != null ?sessionMap.toString() : null);
    GetNumberOfMatches command = new GetNumberOfMatches();
    HashMap<String, Object> convertedParameters = new HashMap<String, Object>();
    if(parameters.containsKey("RegexExp") && parameters.get("RegexExp") != null && parameters.get("RegexExp").get() != null) {
      convertedParameters.put("RegexExp", parameters.get("RegexExp").get());
      if(convertedParameters.get("RegexExp") !=null && !(convertedParameters.get("RegexExp") instanceof Pattern)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","RegexExp", "Pattern", parameters.get("RegexExp").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("RegexExp") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","RegexExp"));
    }

    if(parameters.containsKey("TheText") && parameters.get("TheText") != null && parameters.get("TheText").get() != null) {
      convertedParameters.put("TheText", parameters.get("TheText").get());
      if(convertedParameters.get("TheText") !=null && !(convertedParameters.get("TheText") instanceof String)) {
        throw new BotCommandException(MESSAGES_GENERIC.getString("generic.UnexpectedTypeReceived","TheText", "String", parameters.get("TheText").get().getClass().getSimpleName()));
      }
    }
    if(convertedParameters.get("TheText") == null) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.validation.notEmpty","TheText"));
    }

    try {
      Optional<Value> result =  Optional.ofNullable(command.action((Pattern)convertedParameters.get("RegexExp"),(String)convertedParameters.get("TheText")));
      return logger.traceExit(result);
    }
    catch (ClassCastException e) {
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.IllegalParameters","action"));
    }
    catch (BotCommandException e) {
      logger.fatal(e.getMessage(),e);
      throw e;
    }
    catch (Throwable e) {
      logger.fatal(e.getMessage(),e);
      throw new BotCommandException(MESSAGES_GENERIC.getString("generic.NotBotCommandException",e.getMessage()),e);
    }
  }
}
