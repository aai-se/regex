package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Map;
import java.util.regex.Pattern;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class IFRegexMatchTest {

    IFRegexMatch condition = new IFRegexMatch();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
                {"((?!\\.)[\\w-_.]*[^.])(@\\w+)(\\.\\w+(\\.\\w+)?[^.\\W])","kaumil@google.com,test@hotmail.com,users@yahoomail.com and some other values thatr are not emails",true},
                {"((?!\\.)[\\w-_.]*[^.])(@\\w+)(\\.\\w+(\\.\\w+)?[^.\\W])","No match and some other values thatr are not emails",false}

        };
    }

    @Test(dataProvider = "data")
    public void aTests(String RegexPattern, String InputText, boolean Results){
        Pattern p = Pattern.compile(RegexPattern);
        boolean d = condition.params(p,InputText);
        System.out.println("Res:"+d);
        assertEquals(d,Results);


        /**
        assertEquals(myList.size(),3);
        ArrayList<String> ExpectedRes = new ArrayList<String>();
        ExpectedRes.add("kaumil@google.com");ExpectedRes.add("test@hotmail.com");ExpectedRes.add("users@yahoomail.com");

        ArrayList<String> ActualRes = new ArrayList<String>();
        for(int i=0;i<myList.size();i++){
            //System.out.println("Debug:"+myList.get(i));
            String s = myList.get(i).toString();
            ActualRes.add(s);
        }
        assertEquals(ActualRes,ExpectedRes);
**/


       // assertEquals(output,result);
    }
}
