package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class RemoveNCharacterTest {

    RemoveNCharacter command = new RemoveNCharacter();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
                {2,"abcdefghijklmnopqrstuvwxyz","acdefghijklmnopqrstuvwxyz"},
                {1,"abcdefghijklmnopqrstuvwxyz","bcdefghijklmnopqrstuvwxyz"},
                {-1,"abcdefghijklmnopqrstuvwxyz","abcdefghijklmnopqrstuvwxy"},
                {-3,"abcdefghijklmnopqrstuvwxyz","abcdefghijklmnopqrstuvwyz"}

        };
    }

    @Test(dataProvider = "data")
    public void aTests(Number charToRemove, String InputText, String Res){
        StringValue d = command.action(InputText,charToRemove);
        assertEquals(d.get(),Res);
        System.out.println(d.get());

    }
}
