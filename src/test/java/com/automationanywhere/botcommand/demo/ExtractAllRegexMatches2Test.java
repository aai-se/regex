package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class ExtractAllRegexMatches2Test {

    ExtractAllRegexMatches2 command = new ExtractAllRegexMatches2();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
                {"((?!\\.)[\\w-_.]*[^.])(@\\w+)(\\.\\w+(\\.\\w+)?[^.\\W])","kaumil@google.com,test@hotmail.com,users@yahoomail.com and some other values thatr are not emails",""},
                {"[ ,](\\d+)","?%S0Oj PRbpm 96 82",""}

        };
    }

    @Test(dataProvider = "data")
    public void aTests(String RegexPattern, String InputText, String Results){
        Pattern p = Pattern.compile(RegexPattern);
        ListValue d = command.action(p,InputText);

        List<Value> myList = d.get();

        for(Value v: myList){
            Map<String,Value> myMap = (Map<String, Value>) v.get();
            // = dict.get();

            for (Map.Entry<String,Value> entry : myMap.entrySet()) {
                System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
            }
        }





        /**
        assertEquals(myList.size(),3);
        ArrayList<String> ExpectedRes = new ArrayList<String>();
        ExpectedRes.add("kaumil@google.com");ExpectedRes.add("test@hotmail.com");ExpectedRes.add("users@yahoomail.com");

        ArrayList<String> ActualRes = new ArrayList<String>();
        for(int i=0;i<myList.size();i++){
            //System.out.println("Debug:"+myList.get(i));
            String s = myList.get(i).toString();
            ActualRes.add(s);
        }
        assertEquals(ActualRes,ExpectedRes);
**/


       // assertEquals(output,result);
    }
}
