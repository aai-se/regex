package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Map;
import java.util.regex.Pattern;

import static org.testng.Assert.assertEquals;

@Test(enabled=true)
public class GetNumberOfMatchesTest {

    GetNumberOfMatches command = new GetNumberOfMatches();

    @DataProvider(name = "data")
    public Object[][] dataTobeTested(){

        return new Object[][]{
                {"((?!\\.)[\\w-_.]*[^.])(@\\w+)(\\.\\w+(\\.\\w+)?[^.\\W])","kaumil@google.com,test@hotmail.com,users@yahoomail.com and some other values thatr are not emails",3.0}

        };
    }

    @Test(dataProvider = "data")
    public void aTests(String RegexPattern, String InputText, Double Results){
        Pattern p = Pattern.compile(RegexPattern);
        NumberValue d = command.action(p,InputText);
        System.out.println(d.get());
        assertEquals(d.get(),Results);
    }
}
