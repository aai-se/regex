/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.botcommand.exception.BotCommandException;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;

import static com.automationanywhere.commandsdk.model.DataType.NUMBER;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

/**
 * @author Bren Sapience
 *
 */
@BotCommand
@CommandPkg(label="Remove Characters", name="RemoveCharacters", description="Remove Characters", icon="pkg.svg",
		node_label="Remove Characters",
		return_type= STRING, return_label="Assign the output to variable", return_required=true)

public class RemoveNCharacter {

	private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

	@Execute
	public StringValue action(
			@Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = "Input Text", default_value_type = STRING) @NotEmpty String TheText,
			@Idx(index = "2", type = AttributeType.NUMBER) @Pkg(label = "Character Number to Remove (Ex:-2)", default_value_type = NUMBER, default_value = "") @NotEmpty Number CharToRemove
			)
	{

		if("".equals(TheText)) {throw new BotCommandException(MESSAGES.getString("emptyInputString", "InputText"));}

		int iCharToRemove = CharToRemove.intValue()-1;

		String NewString = TheText;
		if(iCharToRemove<0){
			iCharToRemove = TheText.length() + iCharToRemove +1; // Remember that iCharToRemove is negative!!
		}

		if(iCharToRemove > TheText.length() || iCharToRemove+1 > TheText.length()){
			// Some Error
			throw new BotCommandException(MESSAGES.getString("indexError", TheText));
		}

		NewString = TheText.substring(0,iCharToRemove)+TheText.substring(iCharToRemove+1);

		return new StringValue(NewString);

	}


}
