/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.ListValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.automationanywhere.commandsdk.model.DataType.BOOLEAN;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

/**
 * @author Bren Sapience
 *
 */
@BotCommand
@CommandPkg(label="Extract All Matches", name="ExtractAllRegexMatches", description="Extract All Regex Matches", icon="pkg.svg",
		node_label="Get Regex Matches",
		return_type= DataType.LIST,return_sub_type = DataType.ANY, return_label="List of Dictionaries (with 4 keys where n is the nth match: 'match', 'start', 'end', 'position') ", return_required=true)

public class ExtractAllRegexMatches2 {

	private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

	@Execute
	public ListValue action(
			@Idx(index = "1", type = AttributeType.REGEX) @Pkg(label = "Regex Pattern with Matches", default_value_type = STRING) @NotEmpty Pattern RegexExp,
			@Idx(index = "2", type = AttributeType.TEXT) @Pkg(label = "Input Text", default_value_type = STRING) @NotEmpty String TheText
			)
	{

		ArrayList<DictionaryValue> AllPatternMatches = new ArrayList<DictionaryValue>();

		Matcher m = RegexExp.matcher(TheText);
		int GrpCount = m.groupCount();
		//System.out.println("Group Count:"+GrpCount);
		int idx = 1;
		while (m.find()) {

			Map<String,Value> GlobalMap = new LinkedHashMap();
			String KEY = Integer.toString(idx);
			String KEYMATCH = "match";
			String KEYSTART = "start";
			String KEYEND = "end";
			String KEYCOUNT = "position";

			String MyMatch = m.group();
			String charStart = Integer.toString(m.start());
			String charEnd = Integer.toString(m.end());
			String GroupNumber = Integer.toString(idx);

			Value VALUEMATCH = new StringValue(MyMatch);
			Value VALUESTART = new StringValue(charStart);
			Value VALUEEND = new StringValue(charEnd);
			Value VALUENUM = new StringValue(GroupNumber);

			GlobalMap.put(KEYMATCH,VALUEMATCH);
			GlobalMap.put(KEYSTART,VALUESTART);
			GlobalMap.put(KEYEND,VALUEEND);
			GlobalMap.put(KEYCOUNT,VALUENUM);

			idx++;

			DictionaryValue DictVal = new DictionaryValue(GlobalMap);
			AllPatternMatches.add(DictVal);
		}
		ListValue AllItems = new ListValue();
		AllItems.set(AllPatternMatches);
		return AllItems;

	}


}
