package com.automationanywhere.botcommand.demo;

import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.automationanywhere.commandsdk.model.DataType.STRING;

/**
 * @author Bren Sapience
 *
 */

@BotCommand(commandType = BotCommand.CommandType.Condition)
@CommandPkg(label = "String is Empty", name = "StringisEmpty",
        description = "String is Empty",
        node_label = "String is Empty", icon = "")
public class IFStringEmpty {

    @ConditionTest
    public boolean params(
            @Idx(index = "1", type = AttributeType.TEXT) @Pkg(label = "String to Check", default_value_type = STRING) @NotEmpty String RegexExp
    )
    {
        return RegexExp.isEmpty();
    }
}


