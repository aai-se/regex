package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.automationanywhere.commandsdk.model.AttributeType.TEXT;
import static com.automationanywhere.commandsdk.model.DataType.STRING;

    /**
     * @author Bren Sapience
     *
     */

    @BotCommand(commandType = BotCommand.CommandType.Condition)
    @CommandPkg(label = "Regex match is found", name = "regexmatchisfound", description = "Check if Regex Match is found", node_label = "Regex Match", icon = "")
    public class IFRegexMatch {

        @ConditionTest
        public boolean params(
                @Idx(index = "1", type = AttributeType.REGEX) @Pkg(label = "Regex Pattern with Matches", default_value_type = STRING) @NotEmpty Pattern RegexExp,
                @Idx(index = "2", type = AttributeType.TEXT) @Pkg(label = "Input Text", default_value_type = STRING) @NotEmpty String TheText
        )
        {
            Matcher m = RegexExp.matcher(TheText);
            int GrpCount = m.groupCount();

            int idx = 0;
            while (m.find()) {
                idx++;
            }
            if(idx == 1){return false;}
            if(idx > 1){return true;}
            return false;
        }
    }


