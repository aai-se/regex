/*
 * Copyright (c) 2019 Automation Anywhere.
 * All rights reserved.
 *
 * This software is the proprietary information of Automation Anywhere.
 * You shall use it only in accordance with the terms of the license agreement
 * you entered into with Automation Anywhere.
 */
/**
 *
 */
package com.automationanywhere.botcommand.demo;

import com.automationanywhere.botcommand.data.Value;
import com.automationanywhere.botcommand.data.impl.DictionaryValue;
import com.automationanywhere.botcommand.data.impl.NumberValue;
import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commandsdk.annotations.*;
import com.automationanywhere.commandsdk.annotations.rules.NotEmpty;
import com.automationanywhere.commandsdk.i18n.Messages;
import com.automationanywhere.commandsdk.i18n.MessagesFactory;
import com.automationanywhere.commandsdk.model.AttributeType;
import com.automationanywhere.commandsdk.model.DataType;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.automationanywhere.commandsdk.model.DataType.STRING;

/**
 * @author Bren Sapience
 *
 */
@BotCommand
@CommandPkg(label="Get Number of Matches", name="GetNumberofMatches", description="Get Number of Matches", icon="pkg.svg",
		node_label="# of Matches",
		return_type= DataType.NUMBER, return_label="Number of Regex Matches", return_required=true)

public class GetNumberOfMatches {

	private static final Messages MESSAGES = MessagesFactory.getMessages("com.automationanywhere.botcommand.demo.messages");

	@Execute
	public NumberValue action(
			@Idx(index = "1", type = AttributeType.REGEX) @Pkg(label = "Regex Pattern with Matches", default_value_type = STRING) @NotEmpty Pattern RegexExp,
			@Idx(index = "2", type = AttributeType.TEXT) @Pkg(label = "Input Text", default_value_type = STRING) @NotEmpty String TheText
			)
	{

		Matcher m = RegexExp.matcher(TheText);
		int GrpCount = m.groupCount();

		int idx = 0;
		while (m.find()) {
			idx++;
		}
		return new NumberValue(Integer.toString(idx));

	}


}
